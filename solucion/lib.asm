global intCmp
global intClone
global intDelete
global intPrint
global strCmp
global strClone
global strDelete
global strPrint
global strLen
global arrayNew
global arrayGetSize
global arrayAddLast
global arrayGet
global arrayRemove
global arraySwap
global arrayDelete
global listNew
global listGetSize
global listAddFirst
global listGet
global listRemove
global listSwap
global listClone
global listDelete
global listPrint
global cardNew
global cardGetSuit
global cardGetNumber
global cardGetStacked
global cardCmp
global cardClone
global cardAddStacked
global cardDelete
global cardPrint

section .text

; ** Int **
; int32_t intCmp(int32_t* a, int32_t* b)
;que deberia comparar?

intCmp:
	push rbp
	mov rbp, rsp


	mov esi, [rsi]
	mov edi, [rdi]

	ej1_ciclomenos1:
    cmp edi , esi	;	dil=a , sil=b. cmp hace a-b y si se prende el flag (es decir, SF = 1) se salta con JS (jump short if sign)
    mov eax , -1
    js ej1_fin_ciclo

	ej1_ciclo1:		;	dil=a , sil=b. cmp hace b-a y si no se prende el flag (es decir, SF=0) se salta con JNS (jump short if not sign) 
	cmp edi , esi  
    mov eax , 1
    jns ej1_fin_ciclo

	ej1_ciclo0:		;	se resta a y b, si ZF = 1 entonces se salta a #ej1_fin_ciclo
    cmp esi , edi	
    mov eax , 0
    jz ej1_fin_ciclo


	ej1_fin_ciclo:
	pop rbp  ;final de stackframe
ret ;ret siempre al final del todo