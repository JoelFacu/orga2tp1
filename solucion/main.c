#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <assert.h>
#include <math.h>

#include "lib.h"



int main (void){
int32_t a_variable = 7;
int32_t* a_puntero = &a_variable; //Se guarda en el puntero pepe la dirección de X. Se dice que a_puntero apunta a X. 

int32_t b_variable = 6; 
int32_t* b_puntero = &b_variable;

    int rta = intCmp(a_puntero,b_puntero);
    printf("La respuesta es %d \n", rta);
    return 0;
}

