global intCmp
global intClone
global intDelete
global intPrint
global strCmp
global strClone
global strDelete
global strPrint
global strLen
global arrayNew
global arrayGetSize
global arrayAddLast
global arrayGet
global arrayRemove
global arraySwap
global arrayDelete
global listNew
global listGetSize
global listAddFirst
global listGet
global listRemove
global listSwap
global listClone
global listDelete
global listPrint
global cardNew
global cardGetSuit
global cardGetNumber
global cardGetStacked
global cardCmp
global cardClone
global cardAddStacked
global cardDelete
global cardPrint

section .text

; ** Int **
; int32_t intCmp(int32_t* a, int32_t* b)
;que deberia comparar?
intCmp:
	push rbp
	mov rbp, rsp

	ej1_ciclomenos1:
    cmp dil , sil	;	dil=a , sil=b. cmp hace a-b y si se prende el flag (es decir, SF = 1) se salta con JS (jump short if sign)
    mov eax , -1
    js ej1_fin_ciclo

	ej1_ciclo1:		;	dil=a , sil=b. cmp hace b-a y si no se prende el flag (es decir, SF=0) se salta con JNS (jump short if not sign) 
	cmp sil, dil
    mov eax , 1
    jns ej1_fin_ciclo

	ej1_ciclo0:		;	se resta a y b, si ZF = 1 entonces se salta a #ej1_fin_ciclo
    cmp sil , dil
    mov eax , 0
    jz ej1_fin_ciclo


	ej1_fin_ciclo:
	pop rbp  ;final de stackframe
ret ;ret siempre al final del todo


; int32_t* intClone(int32_t* a)
intClone:
	push rbp
	mov rbp, rsp
	push rbx		 ; pila alineada 8
	push r12		 ; pila alineada 16

	mov edi , 4;
	call malloc
	mov eax , edi

	pop r12
	pop rbx
	pop rbp 
ret

; void intDelete(int32_t* a)
;si lo borro que me da?
intDelete:
	push rbp
	mov rbp, rsp
	push rbx		 ; pila alineada 8
	push r12		 ; pila alineada 16

	mov edi , eax
	call free
	mov eax, edi

	pop r12
	pop rbx
	pop rbp 
ret


; void intPrint(int32_t* a, FILE* pFile)
intPrint:
	push rbp
	mov rbp, rsp

	mov eax , edi
	call printf

	pop rbx
	pop rbp 
ret


; ** String **

; int32_t strCmp(char* a, char* b)
strCmp:
ret

; char* strClone(char* a)
strClone:
	push rbp
	mov rbp, rsp


	mov ecx , 0

	ciclo02:
	cmp ecx, esi
	je  clonar

	inc ecx
	jmp clonar


	clonar02:
	mov edi , ecx;
	call malloc
	mov eax , edi
	
	pop rbx
	pop rbp
ret

; void strDelete(char* a)
strDelete:
ret

; void strPrint(char* a, FILE* pFile)
strPrint:
ret

; uint32_t strLen(char* a)
strLen:
	push rbp
	mov rbp, rsp

	mov ecx , 0
	ciclo03:
	cmp ecx ,esi
	je fin_ciclo02

	inc eax
	jmp ciclo03

	fin_ciclo02:
	pop rbx
	pop rbp
ret

; ** Array **

; array_t* arrayNew(type_t t, uint8_t capacity)
arrayNew:
ret

; uint8_t  arrayGetSize(array_t* a)
arrayGetSize:
ret

; void  arrayAddLast(array_t* a, void* data)
arrayAddLast:
ret

; void* arrayGet(array_t* a, uint8_t i)
arrayGet:
ret

; void* arrayRemove(array_t* a, uint8_t i)
arrayRemove:
ret

; void  arraySwap(array_t* a, uint8_t i, uint8_t j)
arraySwap:
ret

; void  arrayDelete(array_t* a)
arrayDelete:
ret

; ** Lista **

; list_t* listNew(type_t t)
listNew:
ret

; uint8_t  listGetSize(list_t* l)
listGetSize:
ret

; void listAddFirst(list_t* l, void* data)
listAddFirst:
ret

; void* listGet(list_t* l, uint8_t i)
listGet:
ret

; void* listRemove(list_t* l, uint8_t i)
listRemove:
ret

; void  listSwap(list_t* l, uint8_t i, uint8_t j)
listSwap:
ret

; list_t* listClone(list_t* l)
listClone:
ret

; void listDelete(list_t* l)
listDelete:
ret

; ** Card **

; card_t* cardNew(char* suit, int32_t* number)
cardNew:
ret

; char* cardGetSuit(card_t* c)
cardGetSuit:
ret

; int32_t* cardGetNumber(card_t* c) 
cardGetNumber:
ret

; list_t* cardGetStacked(card_t* c)
cardGetStacked:
ret

; int32_t cardCmp(card_t* a, card_t* b)
cardCmp:
ret

; card_t* cardClone(card_t* c)
cardClone:
ret

; void cardAddStacked(card_t* c, card_t* card)
cardAddStacked:
ret

; void cardDelete(card_t* c)
cardDelete:
ret

; void cardPrint(card_t* c, FILE* pFile)
cardPrint:
ret
